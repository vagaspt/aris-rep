﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aris
{
    public partial class FormMenu : Form
    {
        public FormMenu()
        {
            InitializeComponent();
        }

        private void buttonItem14_Click(object sender, EventArgs e)
        {
            frmEmpresas Empresas = new frmEmpresas();
            Empresas.MdiParent = this;
            Empresas.Show();
            
        }

        private void buttonItem15_Click(object sender, EventArgs e)
        {
            frmFuncionarios funcionario = new frmFuncionarios();
            funcionario.MdiParent = this;
            funcionario.Show();

        }

        private void buttonItem16_Click(object sender, EventArgs e)
        {
            frmDepartamento Dept = new frmDepartamento();
            Dept.MdiParent = this;
            Dept.Show();
        }

        private void buttonItem17_Click(object sender, EventArgs e)
        {
            frmCargos cargos = new frmCargos();
            cargos.MdiParent = this;
            cargos.Show();
        }
    }
}
