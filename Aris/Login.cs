﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Prosoft_Development;
using MySql.Data.MySqlClient;

namespace Aris
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            txtpassword.KeyDown += new KeyEventHandler(txtpassword_KeyDown);
            txtusername.KeyDown += new KeyEventHandler(txtusername_KeyDown);
        }

        private void txtusername_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Alogin();
            }
            catch (System.NotImplementedException ex)
            {
                MessageBox.Show(ex.Message);
            }

            

        }

        private void txtpassword_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Alogin();
            }
            catch (System.NotImplementedException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Alogin()

        {

            frmCheckIN Menu = new frmCheckIN();
            Menu.Show();
            this.Hide();
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void groupPanel1_Click(object sender, EventArgs e)
        {

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            try
            {
                Alogin();
            }
            catch (System.NotImplementedException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
