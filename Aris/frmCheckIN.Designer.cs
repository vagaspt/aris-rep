﻿namespace Aris
{
    partial class frmCheckIN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.lblcelular2 = new DevComponents.DotNetBar.LabelX();
            this.lblcelular = new DevComponents.DotNetBar.LabelX();
            this.lbltelefone = new DevComponents.DotNetBar.LabelX();
            this.lblproprietarios = new DevComponents.DotNetBar.LabelX();
            this.lblendereco = new DevComponents.DotNetBar.LabelX();
            this.lblnuit = new DevComponents.DotNetBar.LabelX();
            this.lblnome = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.label1 = new System.Windows.Forms.Label();
            this.cboEmpresa = new System.Windows.Forms.ComboBox();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.Location = new System.Drawing.Point(458, 270);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(111, 33);
            this.buttonX2.TabIndex = 36;
            this.buttonX2.Text = "Sair";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.Location = new System.Drawing.Point(343, 270);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(99, 33);
            this.buttonX1.TabIndex = 35;
            this.buttonX1.Text = "Confirmar";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // lblcelular2
            // 
            this.lblcelular2.BackColor = System.Drawing.Color.Transparent;
            this.lblcelular2.ForeColor = System.Drawing.Color.Black;
            this.lblcelular2.Location = new System.Drawing.Point(133, 241);
            this.lblcelular2.Name = "lblcelular2";
            this.lblcelular2.Size = new System.Drawing.Size(434, 23);
            this.lblcelular2.TabIndex = 34;
            // 
            // lblcelular
            // 
            this.lblcelular.BackColor = System.Drawing.Color.Transparent;
            this.lblcelular.ForeColor = System.Drawing.Color.Black;
            this.lblcelular.Location = new System.Drawing.Point(133, 212);
            this.lblcelular.Name = "lblcelular";
            this.lblcelular.Size = new System.Drawing.Size(434, 23);
            this.lblcelular.TabIndex = 33;
            // 
            // lbltelefone
            // 
            this.lbltelefone.BackColor = System.Drawing.Color.Transparent;
            this.lbltelefone.ForeColor = System.Drawing.Color.Black;
            this.lbltelefone.Location = new System.Drawing.Point(133, 183);
            this.lbltelefone.Name = "lbltelefone";
            this.lbltelefone.Size = new System.Drawing.Size(421, 23);
            this.lbltelefone.TabIndex = 32;
            // 
            // lblproprietarios
            // 
            this.lblproprietarios.BackColor = System.Drawing.Color.Transparent;
            this.lblproprietarios.ForeColor = System.Drawing.Color.Black;
            this.lblproprietarios.Location = new System.Drawing.Point(133, 154);
            this.lblproprietarios.Name = "lblproprietarios";
            this.lblproprietarios.Size = new System.Drawing.Size(434, 23);
            this.lblproprietarios.TabIndex = 31;
            // 
            // lblendereco
            // 
            this.lblendereco.BackColor = System.Drawing.Color.Transparent;
            this.lblendereco.ForeColor = System.Drawing.Color.Black;
            this.lblendereco.Location = new System.Drawing.Point(133, 125);
            this.lblendereco.Name = "lblendereco";
            this.lblendereco.Size = new System.Drawing.Size(434, 23);
            this.lblendereco.TabIndex = 30;
            // 
            // lblnuit
            // 
            this.lblnuit.BackColor = System.Drawing.Color.Transparent;
            this.lblnuit.ForeColor = System.Drawing.Color.Black;
            this.lblnuit.Location = new System.Drawing.Point(133, 96);
            this.lblnuit.Name = "lblnuit";
            this.lblnuit.Size = new System.Drawing.Size(421, 23);
            this.lblnuit.TabIndex = 29;
            // 
            // lblnome
            // 
            this.lblnome.BackColor = System.Drawing.Color.Transparent;
            this.lblnome.ForeColor = System.Drawing.Color.Black;
            this.lblnome.Location = new System.Drawing.Point(133, 67);
            this.lblnome.Name = "lblnome";
            this.lblnome.Size = new System.Drawing.Size(421, 23);
            this.lblnome.TabIndex = 28;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            this.labelX7.Location = new System.Drawing.Point(19, 241);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(108, 23);
            this.labelX7.TabIndex = 27;
            this.labelX7.Text = "Celular 2 :";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            this.labelX6.Location = new System.Drawing.Point(19, 212);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(108, 23);
            this.labelX6.TabIndex = 26;
            this.labelX6.Text = "Celular :";
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            this.labelX5.Location = new System.Drawing.Point(19, 183);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(108, 23);
            this.labelX5.TabIndex = 25;
            this.labelX5.Text = "Telefone :";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            this.labelX4.Location = new System.Drawing.Point(19, 154);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(108, 23);
            this.labelX4.TabIndex = 24;
            this.labelX4.Text = "Proprietarios :";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            this.labelX3.Location = new System.Drawing.Point(19, 96);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(58, 23);
            this.labelX3.TabIndex = 23;
            this.labelX3.Text = "Nuit :";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            this.labelX2.Location = new System.Drawing.Point(19, 125);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(108, 23);
            this.labelX2.TabIndex = 22;
            this.labelX2.Text = "Endereco :";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            this.labelX1.Location = new System.Drawing.Point(19, 67);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 21;
            this.labelX1.Text = "Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(15, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Selecione a Entidade";
            // 
            // cboEmpresa
            // 
            this.cboEmpresa.BackColor = System.Drawing.SystemColors.Window;
            this.cboEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEmpresa.FormattingEnabled = true;
            this.cboEmpresa.Location = new System.Drawing.Point(197, 14);
            this.cboEmpresa.Name = "cboEmpresa";
            this.cboEmpresa.Size = new System.Drawing.Size(370, 21);
            this.cboEmpresa.TabIndex = 19;
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.cboEmpresa);
            this.groupPanel1.Controls.Add(this.buttonX2);
            this.groupPanel1.Controls.Add(this.label1);
            this.groupPanel1.Controls.Add(this.buttonX1);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.lblcelular2);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.lblcelular);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.lbltelefone);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.Controls.Add(this.lblproprietarios);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.lblendereco);
            this.groupPanel1.Controls.Add(this.labelX6);
            this.groupPanel1.Controls.Add(this.lblnuit);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.Controls.Add(this.lblnome);
            this.groupPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupPanel1.Location = new System.Drawing.Point(0, 0);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(625, 334);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.groupPanel1.TabIndex = 37;
            // 
            // frmCheckIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 334);
            this.Controls.Add(this.groupPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCheckIN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.LabelX lblcelular2;
        private DevComponents.DotNetBar.LabelX lblcelular;
        private DevComponents.DotNetBar.LabelX lbltelefone;
        private DevComponents.DotNetBar.LabelX lblproprietarios;
        private DevComponents.DotNetBar.LabelX lblendereco;
        private DevComponents.DotNetBar.LabelX lblnuit;
        private DevComponents.DotNetBar.LabelX lblnome;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboEmpresa;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
    }
}